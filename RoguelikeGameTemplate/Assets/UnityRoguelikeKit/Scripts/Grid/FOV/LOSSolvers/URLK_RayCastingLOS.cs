using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

//DISCLAIMER:
//This is a port of Eben Howard's SquidLib (in Java).
//

/**
 * Uses a series of rays internal to the start and end point to determine
 * visibility.
 *
 * @author Eben Howard - http://squidpony.com - howard@squidpony.com
 * @author P�l Trefall - URLK - admin@ptrefall.com
 */
namespace URLK
{
    public class RayCastingLOS : LOSSolver
    {
        //Queue<Vector2> path;
        //private float gap = 0.4f;//how much gap to leave from the edges when tracing rays
        //private float step = 0.1f;//the size of step to take when walking out rays

        /**
         * Creates a new instance of this solver which uses the provided step
         * distance and gap distance when performing calculations.
         *
         * @param step
         * @param gap
         */
        public RayCastingLOS(float step, float gap)
        {
            /*this.step = step;
            this.gap = gap;*/
        }

        /**
         * Creates an instance of this solver which uses the default gap and step.
         */
        public RayCastingLOS()
        {
        }

        public bool isReachable(float[,] resistanceMap, int startx, int starty, int targetx, int targety, float force, float decay, RadiusStrategy radiusStrategy)
        {
            //path = new Queue<Vector2>();
            //float maxRadius = force/decay;
            /*float x1 = startx + 0.5f;
            float y1 = starty + 0.5f;
            float x2 = targetx + 0.5f;
            float y2 = targety + 0.5f;*/


            //double angle = Math.Atan2(y2 - y1, x2 - x1);
            return false;
        }

        public bool isReachable(float[,] resistanceMap, int startx, int starty, int targetx, int targety)
        {
            return isReachable(resistanceMap, startx, starty, targetx, targety, 1, 0, new BasicRadiusStrategy(BasicRadiusStrategy.STRATEGY.CIRCLE));
        }

        public Queue<Vector2> getLastPath()
        {
            throw new System.InvalidOperationException("Not supported yet.");
        }
    }
}
