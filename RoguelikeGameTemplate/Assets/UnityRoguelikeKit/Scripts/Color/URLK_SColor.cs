
using System;
using UnityEngine;

//DISCLAIMER:
//This is a port of Eben Howard's SquidLib (in Java).
//

/**
 * Allows for the use of custom colors with custom names.
 *
 * These colors are comparable for equality but the ordering of them is based on
 * their hex values.
 *
 * Has some built in palettes available as SColor arrays.
 *
 * @author Eben Howard - http://squidpony.com - howard@squidpony.com
 * @author P�l Trefall - URLK - admin@ptrefall.com
 */

namespace URLK
{
    public class SColor
    {
        private Color32 impl;
        private string name = "Unnamed";

        public string Name { get { return name; } }
        public byte r { get { return impl.r; } }
        public byte g { get { return impl.g; } }
        public byte b { get { return impl.b; } }
        public byte a { get { return impl.a; } }

        /**
         * A constructor with no passed values. Builds a white (opaque all channels)
         * object.
         */
        public SColor()
        {
            //builds white
            setRGBA(SColor.WHITE.getRGBA());
        }

        /**
         * Creates a new color that's the same value as the passed in color.
         *
         * @param color
         */

        public SColor(SColor color)
        {
            setRGBA(color.getRGBA());
            name = color.name;
        }

        /**
         * Creates a new color that's the same value as the passed in color and
         * given the provided name.
         *
         * @param color
         * @param name
         */

        public SColor(SColor color, string name)
        {
            setRGBA(color.getRGBA());
            this.name = name;
        }

        /**
     * Creates a new color that has the given combined RGB value.
     *
     * @param colorValue
     */
        public SColor(int colorValue)
        {
            setRGB(HexToRGB(colorValue));
        }

        /**
         * Creates a new color that has the given combined RGB value and the
         * provided name.
         *
         * @param colorValue
         * @param name
         */
        public SColor(int colorValue, String name)
        {
            this.name = name;
            setRGB(HexToRGB(colorValue));
        }

        /**
         * Creates a new color that has the given RGB values.
         *
         * @param rgb
         */

        public SColor(Vector3 rgb)
        {
            impl = new Color32(Convert.ToByte(rgb.x * 255.0f), Convert.ToByte(rgb.y * 255.0f), Convert.ToByte(rgb.z * 255.0f), 255);
        }

        /**
         * Creates a new color that has the given RGB values.
         *
         * @param rgb
         */

        public SColor(Vector3 rgb, string name)
        {
            this.name = name;
            impl = new Color32(Convert.ToByte(rgb.x * 255.0f), Convert.ToByte(rgb.y * 255.0f), Convert.ToByte(rgb.z * 255.0f), 255);
        }

        /**
         * Creates a new color that has the given RGBA values.
         *
         * @param rgba
         */

        public SColor(Vector4 rgba)
        {
            impl = new Color32(Convert.ToByte(rgba.x * 255.0f), Convert.ToByte(rgba.y * 255.0f), Convert.ToByte(rgba.z * 255.0f),
                               Convert.ToByte(rgba.w * 255.0f));
        }

        /**
         * Creates a new color that has the given RGBA values.
         *
         * @param rgba
         */

        public SColor(Vector4 rgba, string name)
        {
            this.name = name;
            impl = new Color32(Convert.ToByte(rgba.x * 255.0f), Convert.ToByte(rgba.y * 255.0f), Convert.ToByte(rgba.z * 255.0f),
                               Convert.ToByte(rgba.w * 255.0f));
        }

        /**
         * Creates a new color that has the given RGB values.
         *
         * @param r
         * @param g
         * @param b
         */

        public SColor(int r, int g, int b)
        {
            impl = new Color32(Convert.ToByte(r), Convert.ToByte(g), Convert.ToByte(b), 1);
        }

        /**
         * Creates a new color that has the given RGB values and the provided name.
         *
         * @param r
         * @param g
         * @param b
         * @param name
         */

        public SColor(int r, int g, int b, string name)
        {
            this.name = name;
            impl = new Color32(Convert.ToByte(r), Convert.ToByte(g), Convert.ToByte(b), 1);
        }

        /**
         * Creates a new color that has the given RGB values.
         *
         * @param r
         * @param g
         * @param b
         */

        public SColor(int r, int g, int b, int a)
        {
            impl = new Color32(Convert.ToByte(r), Convert.ToByte(g), Convert.ToByte(b), Convert.ToByte(a));
        }

        /**
         * Creates a new color that has the given RGB values and the provided name.
         *
         * @param r
         * @param g
         * @param b
         * @param name
         */

        public SColor(int r, int g, int b, int a, string name)
        {
            this.name = name;
            impl = new Color32(Convert.ToByte(r), Convert.ToByte(g), Convert.ToByte(b), Convert.ToByte(a));
        }

        public SColor(Color32 color)
        {
            impl = color;
        }

        public SColor(Color32 color, string name)
        {
            this.name = name;
            impl = color;
        }

        public SColor(Color color)
        {
            impl = new Color32(Convert.ToByte(color.r * 255.0f), Convert.ToByte(color.g * 255.0f), Convert.ToByte(color.b * 255.0f), 
                Convert.ToByte(color.a * 255.0f));
        }

        public SColor(Color color, string name)
        {
            this.name = name;
            impl = new Color32(Convert.ToByte(color.r * 255.0f), Convert.ToByte(color.g * 255.0f), Convert.ToByte(color.b * 255.0f), 
                Convert.ToByte(color.a * 255.0f));
        }

        public string toString()
        {
            return name;
        }

        public string getName()
        {
            return name;
        }

        public Vector3 getRGB()
        {
            return new Vector3(impl.r / 255.0f, impl.g / 255.0f, impl.b / 255.0f);
        }

        public Vector3 getRGBA()
        {
            return new Vector4(impl.r / 255.0f, impl.g / 255.0f, impl.b / 255.0f, impl.a / 255.0f);
        }

        public Color32 getColor32()
        {
            return impl;
        }

        public int getHex()
        {
            return (impl.r << 16) | (impl.g << 8) | (impl.b);
        }

        public void setRGB(Vector3 rgb)
        {
            impl = new Color32(Convert.ToByte(rgb.x * 255.0f), Convert.ToByte(rgb.y * 255.0f), Convert.ToByte(rgb.z * 255.0f), 255);
        }

        public void setRGBA(Vector4 rgba)
        {
            impl = new Color32(Convert.ToByte(rgba.x * 255.0f), Convert.ToByte(rgba.y * 255.0f), Convert.ToByte(rgba.z * 255.0f),
                               Convert.ToByte(rgba.w * 255.0f));
        }

        public bool equals(SColor other)
        {
            return getRGB() == other.getRGB();
        }

        public static Vector3 HexToRGB(int hexcolor)
        {
            var r = ((hexcolor & 0xFF0000) >> 16)/255.0f;
            var g = ((hexcolor & 0x00FF00) >> 8)/255.0f;
            var b = ((hexcolor & 0x0000FF))/255.0f;
            return new Vector3(r,g,b);
        }

        public static Vector4 HexToRGBA(int hexcolor)
        {
            var r = ((hexcolor & 0xFF0000) >> 16)/255.0f;
            var g = ((hexcolor & 0x00FF00) >> 8)/255.0f;
            var b = ((hexcolor & 0x0000FF))/255.0f;
            return new Vector4(r,g,b,1);
        }

        public static SColor HexToSColor(int hexcolor)
        {
            var r = ((hexcolor & 0xFF0000) >> 16);
            var g = ((hexcolor & 0x00FF00) >> 8);
            var b = ((hexcolor & 0x0000FF));
            return new SColor(r,g,b);
        }

        public static SColor HexToSColor(int hexcolor, string name)
        {
            var r = ((hexcolor & 0xFF0000) >> 16);
            var g = ((hexcolor & 0x00FF00) >> 8);
            var b = ((hexcolor & 0x0000FF));
            return new SColor(r, g, b, name);
        }

        public static SColor ALICE_BLUE = new SColor(0xf0f8ff, "Alice Blue");
        public static SColor ALIZARIN = new SColor(0xE32636, "Alizarin");
        public static SColor ALOEWOOD = new SColor(0x6A432D, "Aloewood");
        public static SColor ALOEWOOD_BROWN = new SColor(0x5A6457, "Aloewood Brown");
        public static SColor AMARANTH = new SColor(0xE52B50, "Amaranth");
        public static SColor AMBER = new SColor(0xFFBF00, "Amber");
        public static SColor AMBER_DYE = new SColor(0xCA6924, "Amber Dye");
        public static SColor AMETHYST = new SColor(0x9966CC, "Amethyst");
        public static SColor AMUR_CORK_TREE = new SColor(0xF3C13A, "Amur Cork Tree");
        public static SColor APRICOT = new SColor(0xFBCEB1, "Apricot");
        public static SColor AQUA = new SColor(0x00FFFF, "Aqua");
        public static SColor AQUAMARINE = new SColor(0x7FFFD4, "Aquamarine");
        public static SColor ARMY_GREEN = new SColor(0x4B5320, "Army Green");
        public static SColor ASPARAGUS = new SColor(0x7BA05B, "Asparagus");
        public static SColor ATOMIC_TANGERINE = new SColor(0xFF9966, "Atomic Tangerine");
        public static SColor AUBURN = new SColor(0x6D351A, "Auburn");
        public static SColor AZUL = new SColor(0x007FFF, "Azul");
        public static SColor AZURE = new SColor(0xF0FFFF, "Azure");
        public static SColor BABY_BLUE = new SColor(0xE0FFFF, "Baby Blue");
        public static SColor BAIKO_BROWN = new SColor(0x857C55, "Baiko Brown");
        public static SColor BEIGE = new SColor(0xF5F5DC, "Beige");
        public static SColor BELLFLOWER = new SColor(0x5D3F6A, "Bellflower");
        public static SColor BENI_DYE = new SColor(0x913225, "Beni Dye");
        public static SColor BETEL_NUT_DYE = new SColor(0x352925, "Betel Nut Dye");
        public static SColor BIRCH_BROWN = new SColor(0xB14A30, "Birch Brown");
        public static SColor BISTRE = new SColor(0x3D2B1F, "Bistre");
        public static SColor BLACK = new SColor(0x000000, "Black");
        public static SColor BLACK_CHESTNUT_OAK = new SColor(0x252321, "Black Chestnut Oak");
        public static SColor BLACK_DYE = new SColor(0x171412, "Black Dye");
        public static SColor BLACK_KITE = new SColor(0x351E1C, "Black Kite");
        public static SColor BLOOD = new SColor(0xCF3A24, "Blood");
        public static SColor BLOOD_RED = new SColor(0xF35336, "Blood Red");
        public static SColor BLUE = new SColor(0x0000ff, "Blue");
        public static SColor BLUE_BLACK_CRAYFISH = new SColor(0x62693B, "Blue Black Crayfish");
        public static SColor BLUE_GREEN = new SColor(0x00DDDD, "Blue Green");
        public static SColor BLUE_GREEN_DYE = new SColor(0x3A6960, "Blue Green Dye");
        public static SColor BLUE_VIOLET = new SColor(0x8A2BE2, "Blue Violet");
        public static SColor BLUE_VIOLET_DYE = new SColor(0x2B2028, "Blue Violet Dye");
        public static SColor BOILED_RED_BEAN_BROWN = new SColor(0x542D24, "Boiled Red Bean Brown");
        public static SColor BONDI_BLUE = new SColor(0x0095B6, "Bondi Blue");
        public static SColor BRASS = new SColor(0xB5A642, "Brass");
        public static SColor BREWED_MUSTARD_BROWN = new SColor(0xE68364, "Brewed Mustard Brown");
        public static SColor BRIGHT_GOLD_BROWN = new SColor(0xCB7E1F, "Bright Gold Brown");
        public static SColor BRIGHT_GOLDEN_YELLOW = new SColor(0xFFA400, "Bright Golden Yellow");
        public static SColor BRIGHT_GREEN = new SColor(0x66FF00, "Bright Green");
        public static SColor BRIGHT_PINK = new SColor(0xFF0080, "Bright Pink");
        public static SColor BRIGHT_TURQUOISE = new SColor(0x08E8DE, "Bright Turquoise");
        public static SColor BRILLIANT_ROSE = new SColor(0xFF55A3, "Brilliant Rose");
        public static SColor BRONZE = new SColor(0xCD7F32, "Bronze");
        public static SColor BROWN = new SColor(0x808000, "Brown");
        public static SColor BROWN_RAT_GREY = new SColor(0x4B3C39, "Brown Rat Grey");
        public static SColor BROWNER = new SColor(0x964B00, "Browner");
        public static SColor BRUSHWOOD_DYED = new SColor(0x8C5939, "Brushwood Dyed");
        public static SColor BUFF = new SColor(0xF0DC82, "Buff");
        public static SColor BURGUNDY = new SColor(0x900020, "Burgundy");
        public static SColor BURNT_BAMBOO = new SColor(0x4D3B3C, "Burnt Bamboo");
        public static SColor BURNT_ORANGE = new SColor(0xCC5500, "Burnt Orange");
        public static SColor BURNT_SIENNA = new SColor(0xE97451, "Burnt Sienna");
        public static SColor BURNT_UMBER = new SColor(0x8A3324, "Burnt Umber");
        public static SColor CAMO_GREEN = new SColor(0x78866B, "Camo Green");
        public static SColor CAPE_JASMINE = new SColor(0xFFB95A, "Cape Jasmine");
        public static SColor CAPUT_MORTUUM = new SColor(0x592720, "Caput Mortuum");
        public static SColor CARDINAL = new SColor(0xC41E3A, "Cardinal");
        public static SColor CARMINE = new SColor(0x960018, "Carmine");
        public static SColor CARNATION_PINK = new SColor(0xFFA6C9, "Carnation Pink");
        public static SColor CAROLINA_BLUE = new SColor(0x99BADD, "Carolina Blue");
        public static SColor CARROT_ORANGE = new SColor(0xED9121, "Carrot Orange");
        public static SColor CATTAIL = new SColor(0xB64925, "Cattail");
        public static SColor CELADON = new SColor(0xACE1AF, "Celadon");
        public static SColor CELADON_DYE = new SColor(0x819C8B, "Celadon Dye");
        public static SColor CERISE = new SColor(0xDE3163, "Cerise");
        public static SColor CERULEAN = new SColor(0x007BA7, "Cerulean");
        public static SColor CERULEAN_BLUE = new SColor(0x2A52BE, "Cerulean Blue");
        public static SColor CHARTREUSE = new SColor(0xDFFF00, "Chartreuse");
        public static SColor CHARTREUSE_GREEN = new SColor(0x7FFF00, "Chartreuse Green");
        public static SColor CHERRY_BLOSSOM = new SColor(0xFFB7C5, "Cherry Blossom");
        public static SColor CHERRY_BLOSSOM_DYE = new SColor(0xfcc9b9, "Cherry Blossom Dye");
        public static SColor CHERRY_BLOSSOM_MOUSE = new SColor(0xAC8181, "Cherry Blossom Mouse");
        public static SColor CHESTNUT = new SColor(0xCD5C5C, "Chestnut");
        public static SColor CHESTNUT_LEATHER_BROWN = new SColor(0x60281E, "Chestnut Leather Brown");
        public static SColor CHESTNUT_PLUM = new SColor(0x8B352D, "Chestnut Plum");
        public static SColor CHINESE_TEA_BROWN = new SColor(0xB35C44, "Chinese Tea Brown");
        public static SColor CHINESE_TEA_YELLOW = new SColor(0xB7702D, "Chinese Tea Yellow");
        public static SColor CHOCOLATE = new SColor(0x7B3F00, "Chocolate");
        public static SColor CINNABAR = new SColor(0xE34234, "Cinnabar");
        public static SColor CINNAMON = new SColor(0xD2691E, "Cinnamon");
        public static SColor CLOVE_BROWN = new SColor(0x8F583C, "Clove Brown");
        public static SColor CLOVE_DYED = new SColor(0xC66B27, "Clove Dyed");
        public static SColor COARSE_WOOL = new SColor(0x181B26, "Coarse Wool");
        public static SColor COBALT = new SColor(0x0047AB, "Cobalt");
        public static SColor COCHINEAL_RED = new SColor(0x9D2933, "Cochineal Red");
        public static SColor COLUMBIA_BLUE = new SColor(0x9BDDFF, "Columbia Blue");
        public static SColor COPPER = new SColor(0xB87333, "Copper");
        public static SColor COPPER_ROSE = new SColor(0x996666, "Copper Rose");
        public static SColor CORAL = new SColor(0xFF7F50, "Coral");
        public static SColor CORAL_DYE = new SColor(0xF8674F, "Coral Dye");
        public static SColor CORAL_RED = new SColor(0xFF4040, "Coral Red");
        public static SColor CORN = new SColor(0xFBEC5D, "Corn");
        public static SColor CORN_DYE = new SColor(0xFAA945, "Corn Dye");
        public static SColor CORNFLOWER_BLUE = new SColor(0x6495ED, "Cornflower Blue");
        public static SColor COSMIC_LATTE = new SColor(0xFFF8E7, "Cosmic Latte");
        public static SColor CREAM = new SColor(0xFFFDD0, "Cream");
        public static SColor CRIMSON = new SColor(0xDC143C, "Crimson");
        public static SColor CYAN = new SColor(0x00ffff, "Cyan");
        public static SColor CYPRESS_BARK = new SColor(0x752E23, "Cypress Bark");
        public static SColor CYPRESS_BARK_RED = new SColor(0x6F3028, "Cypress Bark Red");
        public static SColor DARK_BLUE = new SColor(0x0000c8, "Dark Blue");
        public static SColor DARK_BLUE_DYE = new SColor(0x192236, "Dark Blue Dye");
        public static SColor DARK_BLUE_LAPIS_LAZULI = new SColor(0x1B294B, "Dark Blue Lapis Lazuli");
        public static SColor DARK_BROWN = new SColor(0x654321, "Dark Brown");
        public static SColor DARK_CERULEAN = new SColor(0x08457E, "Dark Cerulean");
        public static SColor DARK_CHESTNUT = new SColor(0x986960, "Dark Chestnut");
        public static SColor DARK_CORAL = new SColor(0xCD5B45, "Dark Coral");
        public static SColor DARK_GOLDENROD = new SColor(0xB8860B, "Dark Goldenrod");
        public static SColor DARK_GRAY = new SColor(0x404040, "Dark Gray");
        public static SColor DARK_GREEN = new SColor(0x013220, "Dark Green");
        public static SColor DARK_INDIGO = new SColor(0x614E6E, "Dark Indigo");
        public static SColor DARK_KHAKI = new SColor(0xBDB76B, "Dark Khaki");
        public static SColor DARK_PASTEL_GREEN = new SColor(0x03C03C, "Dark Pastel Green");
        public static SColor DARK_PINK = new SColor(0xE75480, "Dark Pink");
        public static SColor DARK_SCARLET = new SColor(0x560319, "Dark Scarlet");
        public static SColor DARK_RED = new SColor(0x800000, "Dark Red");
        public static SColor DARK_RED_DYE = new SColor(0x23191E, "Dark Red Dye");
        public static SColor DARK_SALMON = new SColor(0xE9967A, "Dark Salmon");
        public static SColor DARK_SLATE_GRAY = new SColor(0x2F4F4F, "Dark Slate Gray");
        public static SColor DARK_SPRING_GREEN = new SColor(0x177245, "Dark Spring Green");
        public static SColor DARK_TAN = new SColor(0x918151, "Dark Tan");
        public static SColor DARK_TURQUOISE = new SColor(0x00CED1, "Dark Turquoise");
        public static SColor DARK_VIOLET = new SColor(0x9400D3, "Dark Violet");
        public static SColor DAWN = new SColor(0xFA7B62, "Dawn");
        public static SColor DAYLILY = new SColor(0xFF8936, "Daylily");
        public static SColor DEAD_MANS_FINGERS_SEAWEED = new SColor(0x524B2A, "Dead Man's Fingers Seaweed");
        public static SColor DECAYING_LEAVES = new SColor(0xD57835, "Decaying Leaves");
        public static SColor DEEP_CERISE = new SColor(0xDA3287, "Deep Cerise");
        public static SColor DEEP_CHESTNUT = new SColor(0xB94E48, "Deep Chestnut");
        public static SColor DEEP_FUCHSIA = new SColor(0xC154C1, "Deep Fuchsia");
        public static SColor DEEP_LILAC = new SColor(0x9955BB, "Deep Lilac");
        public static SColor DEEP_MAGENTA = new SColor(0xCD00CC, "Deep Magenta");
        public static SColor DEEP_PEACH = new SColor(0xFFCBA4, "Deep Peach");
        public static SColor DEEP_PINK = new SColor(0xFF1493, "Deep Pink");
        public static SColor DEEP_PURPLE = new SColor(0x3A243B, "Deep Purple");
        public static SColor DEEP_SCARLET = new SColor(0x7B3B3A, "Deep Scarlet");
        public static SColor DENIM = new SColor(0x1560BD, "Denim");
        public static SColor DISAPPEARING_PURPLE = new SColor(0x3F313A, "Disappearing Purple");
        public static SColor DISTANT_RIVER_BROWN = new SColor(0xCB6649, "Distant River Brown");
        public static SColor DODGER_BLUE = new SColor(0x1E90FF, "Dodger Blue");
        public static SColor DOVE_FEATHER_GREY = new SColor(0x755D5B, "Dove Feather Grey");
        public static SColor DRIED_WEATHERED_BAMBOO = new SColor(0x7D4E2D, "Dried Weathered Bamboo");
        public static SColor DULL_BLUE = new SColor(0x4F4944, "Dull Blue");
        public static SColor EARTHEN_YELLOW = new SColor(0xBE7F51, "Earthen Yellow");
        public static SColor EARTHEN_YELLOW_RED_BROWN = new SColor(0xFF4E20, "Earthen Yellow Red Brown");
        public static SColor ECRU = new SColor(0xC2B280, "Ecru");
        public static SColor EDO_BROWN = new SColor(0xA13D2D, "Edo Brown");
        public static SColor EGG_DYE = new SColor(0xFFA631, "Egg Dye");
        public static SColor EGGSHELL_PAPER = new SColor(0xECBE9F, "Eggshell Paper");
        public static SColor EGYPTIAN_BLUE = new SColor(0x1034A6, "Egyptian Blue");
        public static SColor ELECTRIC_BLUE = new SColor(0x7DF9FF, "Electric Blue");
        public static SColor ELECTRIC_GREEN = new SColor(0x00FF00, "Electric Green");
        public static SColor ELECTRIC_INDIGO = new SColor(0x6600FF, "Electric Indigo");
        public static SColor ELECTRIC_LIME = new SColor(0xCCFF00, "Electric Lime");
        public static SColor ELECTRIC_PURPLE = new SColor(0xBF00FF, "Electric Purple");
        public static SColor EMERALD = new SColor(0x50C878, "Emerald");
        public static SColor EGGPLANT = new SColor(0x614051, "Eggplant");
        public static SColor FADED_CHINESE_TEA_BROWN = new SColor(0x60281E, "Faded Chinese Tea Brown");
        public static SColor FADED_SEN_NO_RIKYUS_TEA = new SColor(0xB0927A, "Faded Den No Rikyu's Tea");
        public static SColor FAKE_PURPLE = new SColor(0x43242A, "Fake Purple");
        public static SColor FALU_RED = new SColor(0x801818, "Falu Red");
        public static SColor FERN_GREEN = new SColor(0x4F7942, "Fern Green");
        public static SColor FINCH_BROWN = new SColor(0x957B38, "Finch Brown");
        public static SColor FIREBRICK = new SColor(0xB22222, "Firebrick");
        public static SColor FLATTERY_BROWN = new SColor(0x6B4423, "Flattery Brown");
        public static SColor FLAX = new SColor(0xEEDC82, "Flax");
        public static SColor FLIRTATIOUS_INDIGO_TEA = new SColor(0x473F2D, "Flirtatious Indigo Tea");
        public static SColor FLORAL_LEAF = new SColor(0xFFB94E, "Floral Leaf");
        public static SColor FOREIGN_CRIMSON = new SColor(0xC91F37, "Foreign Crimson");
        public static SColor FOREST_GREEN = new SColor(0x228B22, "Forest Green");
        public static SColor FOX = new SColor(0x985629, "Fox");
        public static SColor FRAGILE_SEAWEED_BROWN = new SColor(0x2E372E, "Fragile Seaweed Brown");
        public static SColor FRENCH_ROSE = new SColor(0xF64A8A, "French Rose");
        public static SColor FRESH_ONION = new SColor(0x5B8930, "Fresh Onion");
        public static SColor FUCSHIA_PINK = new SColor(0xFF77FF, "Fucshia Pink");
        public static SColor GAMBOGE = new SColor(0xE49B0F, "Gamboge");
        public static SColor GAMBOGE_DYE = new SColor(0xFFB61E, "Gamboge Dye");
        public static SColor GLAZED_PERSIMMON = new SColor(0xD34E36, "Glazed Persimmon");
        public static SColor GOLD = new SColor(0xD4AF37, "Gold");
        public static SColor GOLDEN = new SColor(0xFFD700, "Golden");
        public static SColor GOLDEN_BROWN = new SColor(0x996515, "Golden Brown");
        public static SColor GOLDEN_BROWN_DYE = new SColor(0xC66B27, "Golden Brown Dye");
        public static SColor GOLDEN_FALLEN_LEAVES = new SColor(0xE29C45, "Golden Fallen Leaves");
        public static SColor GOLDEN_OAK = new SColor(0xBB8141, "Golden Oak");
        public static SColor GOLDEN_YELLOW = new SColor(0xFFDF00, "Golden Yellow");
        public static SColor GOLDENROD = new SColor(0xDAA520, "Goldenrod");
        public static SColor GORYEO_STOREROOM = new SColor(0x203838, "Goryeo Storeroom");
        public static SColor GRAPE_MOUSE = new SColor(0x63424B, "Grape Mouse");
        public static SColor GRAY = new SColor(0x808080, "Gray");
        public static SColor GRAY_ASPARAGUS = new SColor(0x465945, "Gray Asparagus");
        public static SColor GREEN = new SColor(0x008000, "Green");
        public static SColor GREENFINCH = new SColor(0xBDA928, "Greenfinch");
        public static SColor GREEN_BAMBOO = new SColor(0x006442, "Green Bamboo");
        public static SColor GREEN_TEA_DYE = new SColor(0x824B35, "Green Tea Dye");
        public static SColor GREEN_YELLOW = new SColor(0xADFF2F, "Green Yellow");
        public static SColor GREYISH_DARK_GREEN = new SColor(0x656255, "Greyish Dark Green");
        public static SColor HALF_PURPLE = new SColor(0x8D608C, "Half Purple");
        public static SColor HAN_PURPLE = new SColor(0x5218FA, "Han Purple");
        public static SColor HARBOR_RAT = new SColor(0x757D75, "Harbor Rat");
        public static SColor HELIOTROPE = new SColor(0xDF73FF, "Heliotrope");
        public static SColor HOLLYWOOD_CERISE = new SColor(0xF400A1, "Hollywood Cerise");
        public static SColor HORSETAIL = new SColor(0x3D5D42, "Horsetail");
        public static SColor HOT_MAGENTA = new SColor(0xFF00CC, "Hot Magenta");
        public static SColor HOT_PINK = new SColor(0xFF69B4, "Hot Pink");
        public static SColor IBIS = new SColor(0x4C221B, "Iris");
        public static SColor IBIS_WING = new SColor(0xF58F84, "Ibis Wing");
        public static SColor INDIGO = new SColor(0x4B0082, "Indigo");
        public static SColor INDIGO_DYE = new SColor(0x00416A, "Indigo Dye");
        public static SColor INDIGO_INK_BROWN = new SColor(0x393432, "Indigo Ink Brown");
        public static SColor INDIGO_WHITE = new SColor(0xEBF6F7, "Indigo White");
        public static SColor INK = new SColor(0x27221F, "Ink");
        public static SColor INSECT_SCREEN = new SColor(0x2D4436, "Insect Screen");
        public static SColor INSIDE_OF_A_BOTTLE = new SColor(0xC6C2B6, "Inside Of A Bottle");
        public static SColor INTERNATIONAL_KLEIN_BLUE = new SColor(0x002FA7, "International Klein Blue");
        public static SColor INTERNATIONAL_ORANGE = new SColor(0xFF4F00, "International Orange");
        public static SColor IRIS = new SColor(0x763568, "Iris");
        public static SColor IRON = new SColor(0x2B3733, "Iron");
        public static SColor IRONHEAD_FLOWER = new SColor(0x344D56, "Ironhead Flower");
        public static SColor IRON_STORAGE = new SColor(0x2B3736, "Iron Storage");
        public static SColor ISLAMIC_GREEN = new SColor(0x009000, "Islamic Green");
        public static SColor IVORY = new SColor(0xFFFFF0, "Ivory");
        public static SColor IWAI_BROWN = new SColor(0x5E5545, "Iwai Brown");
        public static SColor JADE = new SColor(0x00A86B, "Jade");
        public static SColor JAPANESE_INDIGO = new SColor(0x264348, "Japanese Indigo");
        public static SColor JAPANESE_IRIS = new SColor(0x7F5D3B, "Japanese Iris");
        public static SColor JAPANESE_PALE_BLUE = new SColor(0x8c9c76, "Japanese Pale Blue");
        public static SColor JAPANESE_TRIANDRA_GRASS = new SColor(0xE2B13C, "Japanese Triandra Grass");
        public static SColor KELLY_GREEN = new SColor(0x4CBB17, "Kelly Green");
        public static SColor KHAKI = new SColor(0xC3B091, "Khaki");
        public static SColor KIMONO_STORAGE = new SColor(0x3D4C51, "Kimono Storage");
        public static SColor LAPIS_LAZULI = new SColor(0x1F4788, "Lapis Lazuli");
        public static SColor LAVENDER_FLORAL = new SColor(0xB57EDC, "Lavender Floral");
        public static SColor LAVENDER = new SColor(0xE6E6FA, "Lavender");
        public static SColor LAVENDER_BLUE = new SColor(0xCCCCFF, "Lavender Blue");
        public static SColor LAVENDER_BLUSH = new SColor(0xFFF0F5, "Lavender Blush");
        public static SColor LAVENDER_GRAY = new SColor(0xC4C3D0, "Lavender Gray");
        public static SColor LAVENDER_MAGENTA = new SColor(0xEE82EE, "Lavender Magenta");
        public static SColor LAVENDER_PINK = new SColor(0xFBAED2, "Lavender Pink");
        public static SColor LAVENDER_PURPLE = new SColor(0x967BB6, "Lavender Purple");
        public static SColor LAVENDER_ROSE = new SColor(0xFBA0E3, "Lavender Rose");
        public static SColor LAWN_GREEN = new SColor(0x7CFC00, "Lawn Green");
        public static SColor LEGAL_DYE = new SColor(0x2E211B, "Legal Dye");
        public static SColor LEMON = new SColor(0xFDE910, "Lemon");
        public static SColor LEMON_CHIFFON = new SColor(0xFFFACD, "Lemon Chiffon");
        public static SColor LIGHT_BLUE = new SColor(0xADD8E6, "Light Blue");
        public static SColor LIGHT_BLUE_DYE = new SColor(0x48929B, "Light Blue Dye");
        public static SColor LIGHT_BLUE_FLOWER = new SColor(0x1D697C, "Light Blue Flower");
        public static SColor LIGHT_BLUE_SILK = new SColor(0x044F67, "Light Blue Silk");
        public static SColor LIGHT_GRAY = new SColor(0xc0c0c0, "Light Gray");
        public static SColor LIGHT_KHAKI = new SColor(0xF0E68C, "Light Khaki");
        public static SColor LIGHT_LIME = new SColor(0xBFFF00, "Light Lime");
        public static SColor LIGHT_MAROON = new SColor(0xB03060, "Light Maroon");
        public static SColor LIGHT_PINK = new SColor(0xFFB6C1, "Light Pink");
        public static SColor LIGHT_VIOLET = new SColor(0xEE82EE, "Light Violet");
        public static SColor LIGHT_YELLOW_DYE = new SColor(0xF7BB7D, "Light Yellow Dye");
        public static SColor LILAC = new SColor(0xC8A2C8, "Lilac");
        public static SColor LIME = new SColor(0x00FF00, "Lime");
        public static SColor LIME_GREEN = new SColor(0x32CD32, "Lime Green");
        public static SColor LINEN = new SColor(0xFAF0E6, "Linen");
        public static SColor LONG_SPRING = new SColor(0xB95754, "Long Spring");
        public static SColor LOQUAT_BROWN = new SColor(0xAB6134, "Loquat Brown");
        public static SColor LYE = new SColor(0x7F6B5D, "Lye");
        public static SColor MAGENTA_DYE = new SColor(0xCA1F7B, "Magenta Dye");
        public static SColor MAGIC_MINT = new SColor(0xAAF0D1, "Magic Mint");
        public static SColor MAGNOLIA = new SColor(0xF8F4FF, "Magnolia");
        public static SColor MALACHITE = new SColor(0x0BDA51, "Malachite");
        public static SColor MAROON = new SColor(0x800000, "Maroon");
        public static SColor MAGENTA = new SColor(0xff00ff, "Magenta");
        public static SColor MAYA_BLUE = new SColor(0x73C2FB, "Maya Blue");
        public static SColor MAUVE = new SColor(0xE0B0FF, "Mauve");
        public static SColor MAUVE_TAUPE = new SColor(0x915F6D, "Mauve Taupe");
        public static SColor MEAT = new SColor(0xF9906F, "Meat");
        public static SColor MEDIUM_BLUE = new SColor(0x0000CD, "Medium Blue");
        public static SColor MEDIUM_CARMINE = new SColor(0xAF4035, "Medium Carmine");
        public static SColor MEDIUM_CRIMSON = new SColor(0xc93756, "Medium Crimson");
        public static SColor MEDIUM_LAVENDER_MAGENTA = new SColor(0xCC99CC, "Medium Lavender Magenta");
        public static SColor MEDIUM_PURPLE = new SColor(0x9370DB, "Medium Purple");
        public static SColor MEDIUM_SPRING_GREEN = new SColor(0x00FA9A, "Medium Spring Green");
        public static SColor MIDORI = new SColor(0x2A606B, "Midori");
        public static SColor MIDNIGHT_BLUE = new SColor(0x003366, "Midnight Blue");
        public static SColor MINT_GREEN = new SColor(0x98FF98, "Mint Green");
        public static SColor MISTY_ROSE = new SColor(0xFFE4E1, "Misty Rose");
        public static SColor MOSS = new SColor(0x8B7D3A, "Moss");
        public static SColor MOSS_GREEN = new SColor(0xADDFAD, "Moss Green");
        public static SColor MOUNTBATTEN_PINK = new SColor(0x997A8D, "Mountbatten Pink");
        public static SColor MOUSY_INDIGO = new SColor(0x5C544E, "Mousy Indigo");
        public static SColor MOUSY_WISTERIA = new SColor(0x766980, "Mousy Wisteria");
        public static SColor MULBERRY = new SColor(0x59292C, "Mulberry");
        public static SColor MULBERRY_DYED = new SColor(0xC57F2E, "Mulberry Dyed");
        public static SColor MUSTARD = new SColor(0xFFDB58, "Mustard");
        public static SColor MYRTLE = new SColor(0x21421E, "Myrtle");
        public static SColor NAVAJO_WHITE = new SColor(0xFFDEAD, "Navajo White");
        public static SColor NAVY_BLUE = new SColor(0x000080, "Navy Blue");
        public static SColor NAVY_BLUE_BELLFLOWER = new SColor(0x191F45, "Navy Blue Bellflower");
        public static SColor NAVY_BLUE_DYE = new SColor(0x003171, "Navy Blue Dye");
        public static SColor NEW_BRIDGE = new SColor(0x006C7F, "New Bridge");
        public static SColor NIGHTINGALE = new SColor(0x645530, "Nightingale");
        public static SColor NIGHTINGALE_BROWN = new SColor(0x5C4827, "Nightingale Brown");
        public static SColor OCHRE = new SColor(0xCC7722, "Ochre");
        public static SColor OLD_BAMBOO = new SColor(0x5E644F, "Old Bamboo");
        public static SColor OLD_GOLD = new SColor(0xCFB53B, "Old Gold");
        public static SColor OLD_LACE = new SColor(0xFDF5E6, "Old Lace");
        public static SColor OLD_LAVENDER = new SColor(0x796878, "Old Lavender");
        public static SColor OLD_ROSE = new SColor(0xC08081, "Old Rose");
        public static SColor OLIVE = new SColor(0x808000, "Olive");
        public static SColor OLIVE_DRAB = new SColor(0x6B8E23, "Olive Drab");
        public static SColor OLIVINE = new SColor(0x9AB973, "Olivine");
        public static SColor ONANDO = new SColor(0x364141, "Onando");
        public static SColor ONE_KIN_DYE = new SColor(0xf08f90, "One Kin Dye");
        public static SColor OPPOSITE_FLOWER = new SColor(0x4D646C, "Opposite Flower");
        public static SColor ORANGE = new SColor(0xffc800, "Orange");
        public static SColor ORANGE_PEEL = new SColor(0xFFA000, "Orange Peel");
        public static SColor ORANGE_RED = new SColor(0xFF4500, "Orange Red");
        public static SColor ORANGUTAN = new SColor(0xDC3023, "Orangutan");
        public static SColor ORCHID = new SColor(0xDA70D6, "Orchid");
        public static SColor OVERDYED_RED_BROWN = new SColor(0xE35C38, "Overdyed Red Brown");
        public static SColor PALE_BLUE = new SColor(0xAFEEEE, "Pale Blue");
        public static SColor PALE_BROWN = new SColor(0x987654, "Pale Brown");
        public static SColor PALE_CARMINE = new SColor(0xAF4035, "Pale Carmine");
        public static SColor PALE_CHESTNUT = new SColor(0xDDADAF, "Pale Chestnut");
        public static SColor PALE_CORNFLOWER_BLUE = new SColor(0xABCDEF, "Pale Cornflower Blue");
        public static SColor PALE_CRIMSON = new SColor(0xf2666c, "Pale Crimson");
        public static SColor PALE_FALLEN_LEAVES = new SColor(0xAA8736, "Pale Fallen Leaves");
        public static SColor PALE_GREEN_ONION = new SColor(0x749F8D, "Pale Green Onion");
        public static SColor PALE_INCENSE = new SColor(0xFFA565, "Pale Incense");
        public static SColor PALE_MAGENTA = new SColor(0xF984E5, "Pale Magenta");
        public static SColor PALE_OAK = new SColor(0xBBA46D, "Pale Oak");
        public static SColor PALE_PERSIMMON = new SColor(0xFCA474, "Pale Persimmon");
        public static SColor PALE_PINK = new SColor(0xFADADD, "Pale Pink");
        public static SColor PALE_RED_VIOLET = new SColor(0xDB7093, "Pale Red Violet");
        public static SColor PALE_YOUNG_GREEN_ONION = new SColor(0x8DB255, "Pale Young Green Onion");
        public static SColor PAPAYA_WHIP = new SColor(0xFFEFD5, "Papaya Whip");
        public static SColor PASTEL_GREEN = new SColor(0x77DD77, "Pastel Green");
        public static SColor PASTEL_PINK = new SColor(0xFFD1DC, "Pastel Pink");
        public static SColor PATINA = new SColor(0x407A52, "Patina");
        public static SColor PATRINIA_FLOWER = new SColor(0xD9B611, "Patrinia Flower");
        public static SColor PEACH = new SColor(0xFFE5B4, "Peach");
        public static SColor PEACH_DYE = new SColor(0xf47983, "Peach Dye");
        public static SColor PEACH_ORANGE = new SColor(0xFFCC99, "Peach Orange");
        public static SColor PEACH_YELLOW = new SColor(0xFADFAD, "Peach Yellow");
        public static SColor PEAR = new SColor(0xD1E231, "Pear");
        public static SColor PERIWINKLE = new SColor(0xCCCCFF, "Periwinkle");
        public static SColor PERSIAN_BLUE = new SColor(0x1C39BB, "Persian Blue");
        public static SColor PERSIAN_GREEN = new SColor(0x00A693, "Persian Green");
        public static SColor PERSIAN_INDIGO = new SColor(0x32127A, "Persian Indigo");
        public static SColor PERSIAN_RED = new SColor(0xCC3333, "Persian Red");
        public static SColor PERSIAN_PINK = new SColor(0xF77FBE, "Persian Pink");
        public static SColor PERSIAN_ROSE = new SColor(0xFE28A2, "Persian Rose");
        public static SColor PERSIMMON = new SColor(0xEC5800, "Persimmon");
        public static SColor PERSIMMON_JUICE = new SColor(0x934337, "Persimmon Juice");
        public static SColor PIGMENT_BLUE = new SColor(0x333399, "Pigment Blue");
        public static SColor PINE_GREEN = new SColor(0x01796F, "Pine Green");
        public static SColor PINE_NEEDLE = new SColor(0x454D32, "Pine Needle");
        public static SColor PINK = new SColor(0xffafaf, "Pink");
        public static SColor PINK_ORANGE = new SColor(0xFF9966, "Pink Orange");
        public static SColor PLAIN_MOUSE = new SColor(0x6E5F57, "Plain Mosue");
        public static SColor PLATINUM = new SColor(0xE5E4E2, "Platinum");
        public static SColor PLUM = new SColor(0xCC99CC, "Plum");
        public static SColor PLUM_BLOSSOM_MOUSE = new SColor(0x97645A, "Plum Blossom Mouse");
        public static SColor PLUM_DYED = new SColor(0xFA9258, "Plum Dyed");
        public static SColor PLUM_PURPLE = new SColor(0x8F4155, "Plum Purple");
        public static SColor POLISHED_BROWN = new SColor(0x9F5233, "Polished Brown");
        public static SColor POWDER_BLUE = new SColor(0xB0E0E6, "Powder Blue");
        public static SColor PRUSSIAN_BLUE = new SColor(0x003153, "Prussian Blue");
        public static SColor PSYCHEDELIC_PURPLE = new SColor(0xDD00FF, "Psychedelic Purple");
        public static SColor PUCE = new SColor(0xCC8899, "Puce");
        public static SColor PUMPKIN = new SColor(0xFF7518, "Pumpkin");
        public static SColor PURE_CRIMSON = new SColor(0xC3272B, "Pure Crimson");
        public static SColor PURPLE = new SColor(0x800080, "Purple");
        public static SColor PURPLE_DYE = new SColor(0x4F284B, "Purple Dye");
        public static SColor PURPLE_KITE = new SColor(0x512C31, "Purple Kite");
        public static SColor PURPLE_TAUPE = new SColor(0x50404D, "Purple Taupe");
        public static SColor RABBIT_EAR_IRIS = new SColor(0x491E3C, "Rabbit Ear Iris");
        public static SColor RAPEBLOSSOM_BROWN = new SColor(0xE3B130, "Rapeblossom Brown");
        public static SColor RAPESEED_OIL = new SColor(0xA17917, "Rapeseed Oil");
        public static SColor RAW_UMBER = new SColor(0x734A12, "Raw Umber");
        public static SColor RAZZMATAZZ = new SColor(0xE30B5C, "Razzamatazz");
        public static SColor RED = new SColor(0xff0000, "Red");
        public static SColor RED_BEAN = new SColor(0x672422, "Red Bean");
        public static SColor RED_BIRCH = new SColor(0x9D2B22, "Red Birch");
        public static SColor RED_INCENSE = new SColor(0xF07F5E, "Red Incense");
        public static SColor RED_DYE_TURMERIC = new SColor(0xFB8136, "Red Dye Turmeric");
        public static SColor RED_KITE = new SColor(0x913228, "Red Kite");
        public static SColor RED_OCHRE = new SColor(0x9F5233, "Red Ochre");
        public static SColor RED_PIGMENT = new SColor(0xED1C24, "Red Pigment");
        public static SColor RED_PLUM = new SColor(0xdb5a6b, "Red Plum");
        public static SColor RED_VIOLET = new SColor(0xC71585, "Red Violet");
        public static SColor RED_WISTERIA = new SColor(0xBB7796, "Red Wisteria");
        public static SColor RICH_CARMINE = new SColor(0xD70040, "Rich Carmine");
        public static SColor RICH_GARDENIA = new SColor(0xF57F4F, "Rich Gardenia");
        public static SColor RINSED_OUT_RED = new SColor(0xFF7952, "Rinsed Out Red");
        public static SColor RIKAN_BROWN = new SColor(0x534A32, "Rikan Brown");
        public static SColor ROBIN_EGG_BLUE = new SColor(0x00CCCC, "Robin Egg Blue");
        public static SColor ROSE = new SColor(0xFF007F, "Rose");
        public static SColor ROSE_MADDER = new SColor(0xE32636, "Rose Madder");
        public static SColor ROSE_TAUPE = new SColor(0x905D5D, "Rose Taupe");
        public static SColor ROYAL_BLUE = new SColor(0x4169E1, "Royal Blue");
        public static SColor ROYAL_PURPLE = new SColor(0x6B3FA0, "Royal Purple");
        public static SColor RUBY = new SColor(0xE0115F, "Ruby");
        public static SColor RUSSET = new SColor(0x80461B, "Russet");
        public static SColor RUST = new SColor(0xB7410E, "Rust");
        public static SColor RUSTED_LIGHT_BLUE = new SColor(0x6A7F7A, "Rusted Light Blue");
        public static SColor RUSTY_CELADON = new SColor(0x898A74, "Rusty Celadon");
        public static SColor RUSTY_STORAGE = new SColor(0x455859, "Rusty Storage");
        public static SColor RUSTY_STOREROOM = new SColor(0x3a403b, "Rusty Storeroom");
        public static SColor SAFETY_ORANGE = new SColor(0xFF6600, "Safety Orange");
        public static SColor SAFFLOWER = new SColor(0x5A4F74, "Safflower");
        public static SColor SAFFRON = new SColor(0xF4C430, "Saffron");
        public static SColor SALMON = new SColor(0xFF8C69, "Salmon");
        public static SColor SANDY_BROWN = new SColor(0xF4A460, "Sandy Brown");
        public static SColor SANGRIA = new SColor(0x92000A, "Sangria");
        public static SColor SAPPHIRE = new SColor(0x082567, "Sapphire");
        public static SColor SAPPANWOOD = new SColor(0x7E2639, "Sappanwood");
        public static SColor SAPPANWOOD_INCENSE = new SColor(0xA24F46, "Sappanwood Incense");
        public static SColor SAWTOOTH_OAK = new SColor(0xEC956C, "Sawtooth Oak");
        public static SColor SCARLET = new SColor(0xFF2400, "Scarlet");
        public static SColor SCHOOL_BUS_YELLOW = new SColor(0xFFD800, "School Bus Yellow");
        public static SColor SCORTCHED_BROWN = new SColor(0x351F19, "Scortched Brown");
        public static SColor SEA_GREEN = new SColor(0x2E8B57, "Sea Green");
        public static SColor SEASHELL = new SColor(0xFFF5EE, "Seashell");
        public static SColor SELECTIVE_YELLOW = new SColor(0xFFBA00, "Selective Yellow");
        public static SColor SEN_NO_RIKYUS_TEA = new SColor(0x826B58, "Sen No Riyu's Tea");
        public static SColor SEPIA = new SColor(0x704214, "Sepia");
        public static SColor SHAMROCK_GREEN = new SColor(0x009E60, "Shamrock Green");
        public static SColor SHOCKING_PINK = new SColor(0xFC0FC0, "Shocking Pink");
        public static SColor SHRIMP_BROWN = new SColor(0x5E2824, "Shrimp Brown");
        public static SColor SILK_CREPE_BROWN = new SColor(0x354E4B, "Silk Crepe Brown");
        public static SColor SILVER = new SColor(0xC0C0C0, "Silver");
        public static SColor SILVER_GREY = new SColor(0x97867C, "Silver Grey");
        public static SColor SILVERED_RED = new SColor(0xBC2D29, "Silvered Red");
        public static SColor SIMMERED_SEAWEED = new SColor(0x4C3D30, "Simmered Seawead");
        public static SColor SISKIN_SPROUT_YELLOW = new SColor(0x7A942E, "Siskin Sprout Yellow");
        public static SColor SKY = new SColor(0x4D8FAC, "Sky");
        public static SColor SKY_BLUE = new SColor(0x87CEEB, "Sky Blue");
        public static SColor SLATE_GRAY = new SColor(0x708090, "Slate Gray");
        public static SColor SMALT = new SColor(0x003399, "Smalt");
        public static SColor SOOTY_BAMBOO = new SColor(0x593A27, "Sooty Bamboo");
        public static SColor SOOTY_WILLOW_BAMBOO = new SColor(0x4D4B3A, "Sooty Willow Bamboo");
        public static SColor SPARROW_BROWN = new SColor(0x8C4736, "Sparrow Brown");
        public static SColor SPRING_BUD = new SColor(0xA7FC00, "Spring Bud");
        public static SColor SPRING_GREEN = new SColor(0x00FF7F, "Spring Green");
        public static SColor STAINED_RED = new SColor(0x78779B, "Stained Red");
        public static SColor STEAMED_CHESTNUT = new SColor(0xD3B17D, "Steamed Chestnut");
        public static SColor STEEL_BLUE = new SColor(0x4682B4, "Steel Blue");
        public static SColor STOREROOM_BROWN = new SColor(0x3D4035, "Storeroom Brown");
        public static SColor STYLISH_PERSIMMON = new SColor(0xFFA26B, "Stylish Persimnmon");
        public static SColor SUMAC = new SColor(0x592B1F, "Sumac");
        public static SColor SUMAC_DYED = new SColor(0xE08A1E, "Sumac Dyed");
        public static SColor TAN = new SColor(0xD2B48C, "Tan");
        public static SColor TANGERINE = new SColor(0xF28500, "Tangerine");
        public static SColor TANGERINE_YELLOW = new SColor(0xFFCC00, "Tangerine Yellow");
        public static SColor TATARIAN_ASTER = new SColor(0x976E9A, "Tatarian Aster");
        public static SColor TAUPE = new SColor(0x483C32, "Taupe");
        public static SColor TAWNY = new SColor(0xCD5700, "Tawny");
        public static SColor TEA_GARDEN_CONTEMPLATION = new SColor(0x665343, "Tead Garden Contemplation");
        public static SColor TEA_GREEN = new SColor(0xD0F0C0, "Tea Green");
        public static SColor TEA_ORANGE = new SColor(0xF88379, "Tea Orange");
        public static SColor TEA_ROSE = new SColor(0xF4C2C2, "Tea Rose");
        public static SColor TEAL = new SColor(0x008080, "Teal");
        public static SColor TERRA_COTTA = new SColor(0xE2725B, "Terra Cotta");
        public static SColor THIN_VIOLET = new SColor(0xA87CA0, "Thin Violet");
        public static SColor THISTLE = new SColor(0xD8BFD8, "Thistle");
        public static SColor THOUSAND_HERB = new SColor(0x317589, "Thousand Herb");
        public static SColor THOUSAND_YEAR_OLD_BROWN = new SColor(0x3B3429, "Thousand Year Old Brown");
        public static SColor THOUSAND_YEAR_OLD_GREEN = new SColor(0x374231, "Thousand Year Old Green");
        public static SColor THRICE_DYED_CRIMSON = new SColor(0xF7665A, "Thrice Dyed Crimson");
        public static SColor TOMATO = new SColor(0xFF6347, "Tomato");
        public static SColor TREE_PEONY = new SColor(0xA4345D, "Tree Peony");
        public static SColor TRUE_PINK = new SColor(0xFFC0CB, "True Pink");
        public static SColor TRUE_RED = new SColor(0x8F1D21, "True Red");
        public static SColor TURMERIC = new SColor(0xE69B3A, "Turmeric");
        public static SColor TURQUOISE = new SColor(0x30D5C8, "Turquoise");
        public static SColor TYRIAN_PURPLE = new SColor(0x66023C, "Tyrian Purple");
        public static SColor ULTRAMARINE = new SColor(0x120A8F, "Ultramarine");
        public static SColor ULTRAMARINE_DYE = new SColor(0x5D8CAE, "Ultramarine Dye");
        public static SColor UNBLEACHED_SILK = new SColor(0xFFDDCA, "Unbleached Silk");
        public static SColor UNDRIED_WALL = new SColor(0x785E49, "Undried Wall");
        public static SColor VANISHING_RED_MOUSE = new SColor(0x44312E, "Vanishing Red Mouse");
        public static SColor VEGAS_GOLD = new SColor(0xC5B358, "Vegas Gold");
        public static SColor VELVET = new SColor(0x224634, "Velvet");
        public static SColor VERMILION = new SColor(0xE34234, "Vermillion");
        public static SColor VINE_GRAPE = new SColor(0x6D2B50, "Vine Grape");
        public static SColor VIOLET = new SColor(0x8B00FF, "Violet");
        public static SColor VIOLET_DYE = new SColor(0x5B3256, "Violet Dye");
        public static SColor VIRIDIAN = new SColor(0x40826D, "Viridian");
        public static SColor WALNUT = new SColor(0x9F7462, "Walnut");
        public static SColor WASHED_OUT_CRIMSON = new SColor(0xffb3a7, "Washed Out Crimson");
        public static SColor WASHED_OUT_PERSIMMON = new SColor(0xEC8254, "Washed Out Persimmon");
        public static SColor WATER = new SColor(0x86ABA5, "Water");
        public static SColor WATER_PERSIMMON = new SColor(0xB56C60, "Water Persimmon");
        public static SColor WHEAT = new SColor(0xF5DEB3, "Wheat");
        public static SColor WHITE = new SColor(0xffffff, "White");
        public static SColor WHITE_MOUSE = new SColor(0xB9A193, "white Mouse");
        public static SColor WHITE_OAK = new SColor(0xCE9F6F, "White Oak");
        public static SColor WHITE_TEA_DYE = new SColor(0xC48E69, "White Tea Dye");
        public static SColor WHITISH_GREEN = new SColor(0xA5BA93, "Whitish Green");
        public static SColor WILLOW_DYE = new SColor(0x8C9E5E, "Willow Dye");
        public static SColor WILLOW_GREY = new SColor(0x817B69, "Willow Grey");
        public static SColor WILLOW_TEA = new SColor(0x9C8A4D, "Willow Tea");
        public static SColor WILTED_LAWN_CLIPPINGS = new SColor(0xAB4C3D, "Wilted Lawn Clippings");
        public static SColor WILLOW_LEAVES_UNDERSIDE = new SColor(0xBCB58C, "Willow Leaves Underside");
        public static SColor WISTERIA = new SColor(0xC9A0DC, "Wisteria");
        public static SColor WISTERIA_DYE = new SColor(0x89729E, "Wisteria Dye");
        public static SColor WISTERIA_PURPLE = new SColor(0x875F9A, "Wisteria Purple");
        public static SColor YELLOW = new SColor(0xffff00, "Yellow");
        public static SColor YELLOW_GREEN = new SColor(0x9ACD32, "Yellow Green");
        public static SColor YELLOW_SEA_PINE_BROWN = new SColor(0x896C39, "Yellow Sea Pine Brown");
        public static SColor YOUNG_BAMBOO = new SColor(0x6b9362, "Young Bamboo");
        public static SColor ZINNWALDITE = new SColor(0xEBC2AF, "Zinnwaldite");
        public static SColor TRANSPARENT = new SColor(0x00000000, "Transparent");

        /**
         * This array is loaded with the colors found in the rainbow, in the
         * standard ROYGBIV order.
         */
        public static SColor[] RAINBOW = {
            RED_PIGMENT, ORANGE_PEEL, YELLOW, GREEN, BLUE, INDIGO_DYE, VIOLET};
        /**
         * This array is loaded with the colors from the traditional Japanese
         * Red-Violet Series found at the wikipedia site here:
         * http://en.wikipedia.org/wiki/Traditional_colors_of_Japan
         */
        public static SColor[] RED_VIOLET_SERIES = {
            ONE_KIN_DYE, RED_PLUM, CHERRY_BLOSSOM_DYE, PALE_CRIMSON, PEACH_DYE, MEDIUM_CRIMSON, WASHED_OUT_CRIMSON};
        /**
         * This array is loaded with the colors from the traditional Japanese Red
         * Series found at the wikipedia site here:
         * http://en.wikipedia.org/wiki/Traditional_colors_of_Japan
         */
        public static SColor[] RED_SERIES = {
            IBIS_WING, LONG_SPRING, COCHINEAL_RED, THRICE_DYED_CRIMSON, PLUM_BLOSSOM_MOUSE, PURE_CRIMSON, RED_BEAN,
            SHRIMP_BROWN, DAWN, ORANGUTAN, PERSIMMON_JUICE, RED_KITE, BLACK_KITE, GLAZED_PERSIMMON, EDO_BROWN,
            CYPRESS_BARK, RINSED_OUT_RED, BREWED_MUSTARD_BROWN, OVERDYED_RED_BROWN, CHINESE_TEA_BROWN, FADED_CHINESE_TEA_BROWN,
            CHESTNUT_LEATHER_BROWN, IBIS, CHERRY_BLOSSOM_MOUSE, FOREIGN_CRIMSON, DEEP_SCARLET, WATER_PERSIMMON,
            SAPPANWOOD_INCENSE, TRUE_RED, SILVERED_RED, CHESTNUT_PLUM, CORAL_DYE, WILTED_LAWN_CLIPPINGS, RED_BIRCH,
            CYPRESS_BARK_RED, BLOOD_RED, BLOOD, BENI_DYE, MEAT, RED_INCENSE, EARTHEN_YELLOW_RED_BROWN, DISTANT_RIVER_BROWN,
            BIRCH_BROWN, SPARROW_BROWN, BOILED_RED_BEAN_BROWN};
        /**
         * This array is loaded with the colors from the traditional Japanese Yellow
         * Red Series found at the wikipedia site here:
         * http://en.wikipedia.org/wiki/Traditional_colors_of_Japan
         */
        public static SColor[] YELLOW_RED_SERIES = {
            WALNUT, SUMAC, RICH_GARDENIA, RED_OCHRE, POLISHED_BROWN, STYLISH_PERSIMMON, DAYLILY, RED_DYE_TURMERIC,
            LEGAL_DYE, AMBER_DYE, DECAYING_LEAVES, CLOVE_DYED, BRUSHWOOD_DYED, SOOTY_BAMBOO, EARTHEN_YELLOW, CHINESE_TEA_YELLOW,
            CATTAIL, SCORTCHED_BROWN, WASHED_OUT_PERSIMMON, SAWTOOTH_OAK, GREEN_TEA_DYE, PALE_PERSIMMON, PLUM_DYED,
            CLOVE_BROWN, LOQUAT_BROWN, PALE_INCENSE, GOLDEN_BROWN_DYE, FOX, ALOEWOOD, WHITE_TEA_DYE,
            DRIED_WEATHERED_BAMBOO, FLATTERY_BROWN};
        /**
         * This array is loaded with the colors from the traditional Japanese Yellow
         * Series found at the wikipedia site here:
         * http://en.wikipedia.org/wiki/Traditional_colors_of_Japan
         */
        public static SColor[] YELLOW_SERIES = {
            LIGHT_YELLOW_DYE, EGG_DYE, BRIGHT_GOLD_BROWN, UNDRIED_WALL, CORN_DYE, GOLDEN_OAK, FLORAL_LEAF, TURMERIC,
            FADED_SEN_NO_RIKYUS_TEA, LYE, TEA_GARDEN_CONTEMPLATION, RAPESEED_OIL, NIGHTINGALE_BROWN, JAPANESE_TRIANDRA_GRASS,
            STEAMED_CHESTNUT, FINCH_BROWN, NIGHTINGALE, BRIGHT_GOLDEN_YELLOW, SUMAC_DYED, MULBERRY_DYED, CAPE_JASMINE,
            WHITE_OAK, GAMBOGE_DYE, EGGSHELL_PAPER, GOLDEN_FALLEN_LEAVES, SEN_NO_RIKYUS_TEA, JAPANESE_IRIS,
            SIMMERED_SEAWEED, YELLOW_SEA_PINE_BROWN, RAPEBLOSSOM_BROWN, AMUR_CORK_TREE, PALE_FALLEN_LEAVES, PATRINIA_FLOWER};
        /**
         * This array is loaded with the colors from the traditional Japanese Yellow
         * Green Series found at the wikipedia site here:
         * http://en.wikipedia.org/wiki/Traditional_colors_of_Japan
         */
        public static SColor[] YELLOW_GREEN_SERIES = {
            GREENFINCH, WILLOW_TEA, FLIRTATIOUS_INDIGO_TEA, DEAD_MANS_FINGERS_SEAWEED, BAIKO_BROWN, SISKIN_SPROUT_YELLOW,
            WILLOW_LEAVES_UNDERSIDE, WILLOW_DYE, BLUE_BLACK_CRAYFISH, PALE_OAK, RIKAN_BROWN, MOSS, THOUSAND_YEAR_OLD_BROWN,
            IWAI_BROWN, SOOTY_WILLOW_BAMBOO, PALE_YOUNG_GREEN_ONION, FRESH_ONION, PINE_NEEDLE};
        /**
         * This array is loaded with the colors from the traditional Japanese Blue
         * Green Series found at the wikipedia site here:
         * http://en.wikipedia.org/wiki/Traditional_colors_of_Japan
         */
        public static SColor[] BLUE_GREEN_SERIES = {
            JAPANESE_PALE_BLUE, WILLOW_GREY, THOUSAND_YEAR_OLD_GREEN, WHITISH_GREEN, PATINA, STOREROOM_BROWN, GREYISH_DARK_GREEN,
            INSECT_SCREEN, ALOEWOOD_BROWN, CELADON_DYE, RUSTY_STOREROOM, SILK_CREPE_BROWN, YOUNG_BAMBOO, OLD_BAMBOO,
            MIDORI, RUSTY_CELADON, HORSETAIL, GREEN_BAMBOO, VELVET, FRAGILE_SEAWEED_BROWN, PALE_GREEN_ONION, BLUE_GREEN_DYE,
            IRON, GORYEO_STOREROOM};
        /**
         * This array is loaded with the colors from the traditional Japanese Blue
         * Violet Series found at the wikipedia site here:
         * http://en.wikipedia.org/wiki/Traditional_colors_of_Japan
         */
        public static SColor[] BLUE_VIOLET_SERIES = {
            HARBOR_RAT, IRON_STORAGE, RUSTED_LIGHT_BLUE, LIGHT_BLUE_DYE, RUSTY_STORAGE, JAPANESE_INDIGO, LIGHT_BLUE_FLOWER,
            OPPOSITE_FLOWER, IRONHEAD_FLOWER, SKY, ULTRAMARINE_DYE, COARSE_WOOL, NAVY_BLUE_DYE, STAINED_RED, MOUSY_WISTERIA,
            WISTERIA_DYE, DULL_BLUE, WATER, INSIDE_OF_A_BOTTLE, NEW_BRIDGE, MOUSY_INDIGO, ONANDO, THOUSAND_HERB, LIGHT_BLUE_SILK,
            KIMONO_STORAGE, BLACK_CHESTNUT_OAK, DARK_BLUE_DYE, LAPIS_LAZULI, DARK_BLUE_LAPIS_LAZULI, NAVY_BLUE_BELLFLOWER,
            SAFFLOWER, DARK_INDIGO};
        /**
         * This array is loaded with the colors from the traditional Japanese Violet
         * Series found at the wikipedia site here:
         * http://en.wikipedia.org/wiki/Traditional_colors_of_Japan
         */
        public static SColor[] VIOLET_SERIES = {
            WISTERIA_PURPLE, TATARIAN_ASTER, BLUE_VIOLET_DYE, THIN_VIOLET, VIOLET_DYE, DARK_RED_DYE, RED_WISTERIA,
            DOVE_FEATHER_GREY, VINE_GRAPE, TREE_PEONY, FAKE_PURPLE, SAPPANWOOD, VANISHING_RED_MOUSE, BELLFLOWER,
            DISAPPEARING_PURPLE, DEEP_PURPLE, HALF_PURPLE, PURPLE_DYE, IRIS, RABBIT_EAR_IRIS, GRAPE_MOUSE, BURNT_BAMBOO,
            PLUM_PURPLE, PURPLE_KITE, MULBERRY};
        /**
         * This array is loaded with the colors from the traditional Japanese
         * Achromatic Series found at the wikipedia site here:
         * http://en.wikipedia.org/wiki/Traditional_colors_of_Japan
         */
        public static SColor[] ACHROMATIC_SERIES = {
            UNBLEACHED_SILK, SILVER_GREY, BROWN_RAT_GREY, BETEL_NUT_DYE, BLACK_DYE, WHITE_MOUSE, PLAIN_MOUSE,
            INDIGO_INK_BROWN, INK, INDIGO_WHITE};
        /**
         * This array is loaded with all of the colors defined in SColor, in
         * arbitrary order.
         */
        public static SColor[] FULL_PALLET = {ALICE_BLUE, ALIZARIN, ALOEWOOD, ALOEWOOD_BROWN, AMARANTH, AMBER,
            AMBER_DYE, AMETHYST, AMUR_CORK_TREE, APRICOT, AQUA, AQUAMARINE, ARMY_GREEN, ASPARAGUS, ATOMIC_TANGERINE,
            AUBURN, AZUL, AZURE, BABY_BLUE, BAIKO_BROWN, BEIGE, BELLFLOWER, BENI_DYE, BETEL_NUT_DYE, BIRCH_BROWN,
            BISTRE, BLACK, BLACK_CHESTNUT_OAK, BLACK_DYE, BLACK_KITE, BLOOD, BLOOD_RED, BLUE, BLUE_BLACK_CRAYFISH,
            BLUE_GREEN, BLUE_GREEN_DYE, BLUE_VIOLET, BLUE_VIOLET_DYE, BOILED_RED_BEAN_BROWN, BONDI_BLUE, BRASS,
            BREWED_MUSTARD_BROWN, BRIGHT_GOLD_BROWN, BRIGHT_GOLDEN_YELLOW, BRIGHT_GREEN, BRIGHT_PINK,
            BRIGHT_TURQUOISE, BRILLIANT_ROSE, BRONZE, BROWN, BROWN_RAT_GREY, BROWNER, BRUSHWOOD_DYED, BUFF,
            BURGUNDY, BURNT_BAMBOO, BURNT_ORANGE, BURNT_SIENNA, BURNT_UMBER, CAMO_GREEN, CAPE_JASMINE,
            CAPUT_MORTUUM, CARDINAL, CARMINE, CARNATION_PINK, CAROLINA_BLUE, CARROT_ORANGE, CATTAIL, CELADON,
            CELADON_DYE, CERISE, CERULEAN, CERULEAN_BLUE, CHARTREUSE, CHARTREUSE_GREEN, CHERRY_BLOSSOM,
            CHERRY_BLOSSOM_DYE, CHERRY_BLOSSOM_MOUSE, CHESTNUT, CHESTNUT_LEATHER_BROWN, CHESTNUT_PLUM,
            CHINESE_TEA_BROWN, CHINESE_TEA_YELLOW, CHOCOLATE, CINNABAR, CINNAMON, CLOVE_BROWN, CLOVE_DYED,
            COARSE_WOOL, COBALT, COCHINEAL_RED, COLUMBIA_BLUE, COPPER, COPPER_ROSE, CORAL, CORAL_DYE, CORAL_RED,
            CORN, CORN_DYE, CORNFLOWER_BLUE, COSMIC_LATTE, CREAM, CRIMSON, CYAN, CYPRESS_BARK, CYPRESS_BARK_RED,
            DARK_BLUE, DARK_BLUE_DYE, DARK_BLUE_LAPIS_LAZULI, DARK_BROWN, DARK_CERULEAN, DARK_CHESTNUT, DARK_CORAL,
            DARK_GOLDENROD, DARK_GRAY, DARK_GREEN, DARK_INDIGO, DARK_KHAKI, DARK_PASTEL_GREEN, DARK_PINK,
            DARK_SCARLET, DARK_RED, DARK_RED_DYE, DARK_SALMON, DARK_SLATE_GRAY, DARK_SPRING_GREEN, DARK_TAN,
            DARK_TURQUOISE, DARK_VIOLET, DAWN, DAYLILY, DEAD_MANS_FINGERS_SEAWEED, DECAYING_LEAVES, DEEP_CERISE,
            DEEP_CHESTNUT, DEEP_FUCHSIA, DEEP_LILAC, DEEP_MAGENTA, DEEP_PEACH, DEEP_PINK, DEEP_PURPLE, DEEP_SCARLET,
            DENIM, DISAPPEARING_PURPLE, DISTANT_RIVER_BROWN, DODGER_BLUE, DOVE_FEATHER_GREY, DRIED_WEATHERED_BAMBOO,
            DULL_BLUE, EARTHEN_YELLOW, EARTHEN_YELLOW_RED_BROWN, ECRU, EDO_BROWN, EGG_DYE, EGGSHELL_PAPER,
            EGYPTIAN_BLUE, ELECTRIC_BLUE, ELECTRIC_GREEN, ELECTRIC_INDIGO, ELECTRIC_LIME, ELECTRIC_PURPLE, EMERALD,
            EGGPLANT, FADED_CHINESE_TEA_BROWN, FADED_SEN_NO_RIKYUS_TEA, FAKE_PURPLE, FALU_RED, FERN_GREEN,
            FINCH_BROWN, FIREBRICK, FLATTERY_BROWN, FLAX, FLIRTATIOUS_INDIGO_TEA, FLORAL_LEAF, FOREIGN_CRIMSON,
            FOREST_GREEN, FOX, FRAGILE_SEAWEED_BROWN, FRENCH_ROSE, FRESH_ONION, FUCSHIA_PINK, GAMBOGE, GAMBOGE_DYE,
            GLAZED_PERSIMMON, GOLD, GOLDEN, GOLDEN_BROWN, GOLDEN_BROWN_DYE, GOLDEN_FALLEN_LEAVES, GOLDEN_OAK,
            GOLDEN_YELLOW, GOLDENROD, GORYEO_STOREROOM, GRAPE_MOUSE, GRAY, GRAY_ASPARAGUS, GREEN, GREENFINCH,
            GREEN_BAMBOO, GREEN_TEA_DYE, GREEN_YELLOW, GREYISH_DARK_GREEN, HALF_PURPLE, HAN_PURPLE, HARBOR_RAT,
            HELIOTROPE, HOLLYWOOD_CERISE, HORSETAIL, HOT_MAGENTA, HOT_PINK, IBIS, IBIS_WING, INDIGO, INDIGO_DYE,
            INDIGO_INK_BROWN, INDIGO_WHITE, INK, INSECT_SCREEN, INSIDE_OF_A_BOTTLE, INTERNATIONAL_KLEIN_BLUE,
            INTERNATIONAL_ORANGE, IRIS, IRON, IRONHEAD_FLOWER, IRON_STORAGE, ISLAMIC_GREEN, IVORY, IWAI_BROWN,
            JADE, JAPANESE_INDIGO, JAPANESE_IRIS, JAPANESE_PALE_BLUE, JAPANESE_TRIANDRA_GRASS, KELLY_GREEN, KHAKI,
            KIMONO_STORAGE, LAPIS_LAZULI, LAVENDER_FLORAL, LAVENDER, LAVENDER_BLUE, LAVENDER_BLUSH, LAVENDER_GRAY,
            LAVENDER_MAGENTA, LAVENDER_PINK, LAVENDER_PURPLE, LAVENDER_ROSE, LAWN_GREEN, LEGAL_DYE, LEMON,
            LEMON_CHIFFON, LIGHT_BLUE, LIGHT_BLUE_DYE, LIGHT_BLUE_FLOWER, LIGHT_BLUE_SILK, LIGHT_GRAY, LIGHT_KHAKI,
            LIGHT_LIME, LIGHT_MAROON, LIGHT_PINK, LIGHT_VIOLET, LIGHT_YELLOW_DYE, LILAC, LIME, LIME_GREEN, LINEN,
            LONG_SPRING, LOQUAT_BROWN, LYE, MAGENTA_DYE, MAGIC_MINT, MAGNOLIA, MALACHITE, MAROON, MAGENTA,
            MAYA_BLUE, MAUVE, MAUVE_TAUPE, MEAT, MEDIUM_BLUE, MEDIUM_CARMINE, MEDIUM_CRIMSON,
            MEDIUM_LAVENDER_MAGENTA, MEDIUM_PURPLE, MEDIUM_SPRING_GREEN, MIDORI, MIDNIGHT_BLUE, MINT_GREEN,
            MISTY_ROSE, MOSS, MOSS_GREEN, MOUNTBATTEN_PINK, MOUSY_INDIGO, MOUSY_WISTERIA, MULBERRY, MULBERRY_DYED,
            MUSTARD, MYRTLE, NAVAJO_WHITE, NAVY_BLUE, NAVY_BLUE_BELLFLOWER, NAVY_BLUE_DYE, NEW_BRIDGE, NIGHTINGALE,
            NIGHTINGALE_BROWN, OCHRE, OLD_BAMBOO, OLD_GOLD, OLD_LACE, OLD_LAVENDER, OLD_ROSE, OLIVE, OLIVE_DRAB,
            OLIVINE, ONANDO, ONE_KIN_DYE, OPPOSITE_FLOWER, ORANGE, ORANGE_PEEL, ORANGE_RED, ORANGUTAN, ORCHID,
            OVERDYED_RED_BROWN, PALE_BLUE, PALE_BROWN, PALE_CARMINE, PALE_CHESTNUT, PALE_CORNFLOWER_BLUE,
            PALE_CRIMSON, PALE_FALLEN_LEAVES, PALE_GREEN_ONION, PALE_INCENSE, PALE_MAGENTA, PALE_OAK, PALE_PERSIMMON,
            PALE_PINK, PALE_RED_VIOLET, PALE_YOUNG_GREEN_ONION, PAPAYA_WHIP, PASTEL_GREEN, PASTEL_PINK, PATINA,
            PATRINIA_FLOWER, PEACH, PEACH_DYE, PEACH_ORANGE, PEACH_YELLOW, PEAR, PERIWINKLE, PERSIAN_BLUE,
            PERSIAN_GREEN, PERSIAN_INDIGO, PERSIAN_RED, PERSIAN_PINK, PERSIAN_ROSE, PERSIMMON, PERSIMMON_JUICE,
            PIGMENT_BLUE, PINE_GREEN, PINE_NEEDLE, PINK, PINK_ORANGE, PLAIN_MOUSE, PLATINUM, PLUM,
            PLUM_BLOSSOM_MOUSE, PLUM_DYED, PLUM_PURPLE, POLISHED_BROWN, POWDER_BLUE, PRUSSIAN_BLUE,
            PSYCHEDELIC_PURPLE, PUCE, PUMPKIN, PURE_CRIMSON, PURPLE, PURPLE_DYE, PURPLE_KITE, PURPLE_TAUPE,
            RABBIT_EAR_IRIS, RAPEBLOSSOM_BROWN, RAPESEED_OIL, RAW_UMBER, RAZZMATAZZ, RED, RED_BEAN, RED_BIRCH,
            RED_INCENSE, RED_DYE_TURMERIC, RED_KITE, RED_OCHRE, RED_PIGMENT, RED_PLUM, RED_VIOLET, RED_WISTERIA,
            RICH_CARMINE, RICH_GARDENIA, RINSED_OUT_RED, RIKAN_BROWN, ROBIN_EGG_BLUE, ROSE, ROSE_MADDER, ROSE_TAUPE,
            ROYAL_BLUE, ROYAL_PURPLE, RUBY, RUSSET, RUST, RUSTED_LIGHT_BLUE, RUSTY_CELADON, RUSTY_STORAGE,
            RUSTY_STOREROOM, SAFETY_ORANGE, SAFFLOWER, SAFFRON, SALMON, SANDY_BROWN, SANGRIA, SAPPHIRE, SAPPANWOOD,
            SAPPANWOOD_INCENSE, SAWTOOTH_OAK, SCARLET, SCHOOL_BUS_YELLOW, SCORTCHED_BROWN, SEA_GREEN, SEASHELL,
            SELECTIVE_YELLOW, SEN_NO_RIKYUS_TEA, SEPIA, SHAMROCK_GREEN, SHOCKING_PINK, SHRIMP_BROWN,
            SILK_CREPE_BROWN, SILVER, SILVER_GREY, SILVERED_RED, SIMMERED_SEAWEED, SISKIN_SPROUT_YELLOW, SKY,
            SKY_BLUE, SLATE_GRAY, SMALT, SOOTY_BAMBOO, SOOTY_WILLOW_BAMBOO, SPARROW_BROWN, SPRING_BUD,
            SPRING_GREEN, STAINED_RED, STEAMED_CHESTNUT, STEEL_BLUE, STOREROOM_BROWN, STYLISH_PERSIMMON, SUMAC,
            SUMAC_DYED, TAN, TANGERINE, TANGERINE_YELLOW, TATARIAN_ASTER, TAUPE, TAWNY, TEA_GARDEN_CONTEMPLATION,
            TEA_GREEN, TEA_ORANGE, TEA_ROSE, TEAL, TERRA_COTTA, THIN_VIOLET, THISTLE, THOUSAND_HERB,
            THOUSAND_YEAR_OLD_BROWN, THOUSAND_YEAR_OLD_GREEN, THRICE_DYED_CRIMSON, TOMATO, TREE_PEONY, TRUE_PINK,
            TRUE_RED, TURMERIC, TURQUOISE, TYRIAN_PURPLE, ULTRAMARINE, ULTRAMARINE_DYE, UNBLEACHED_SILK,
            UNDRIED_WALL, VANISHING_RED_MOUSE, VEGAS_GOLD, VELVET, VERMILION, VINE_GRAPE, VIOLET, VIOLET_DYE,
            VIRIDIAN, WALNUT, WASHED_OUT_CRIMSON, WASHED_OUT_PERSIMMON, WATER, WATER_PERSIMMON, WHEAT, WHITE,
            WHITE_MOUSE, WHITE_OAK, WHITE_TEA_DYE, WHITISH_GREEN, WILLOW_DYE, WILLOW_GREY, WILLOW_TEA,
            WILTED_LAWN_CLIPPINGS, WILLOW_LEAVES_UNDERSIDE, WISTERIA, WISTERIA_DYE, WISTERIA_PURPLE, YELLOW,
            YELLOW_GREEN, YELLOW_SEA_PINE_BROWN, YOUNG_BAMBOO, ZINNWALDITE};
    }
}
