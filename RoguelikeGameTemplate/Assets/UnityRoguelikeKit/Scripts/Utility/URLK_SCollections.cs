using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

//DISCLAIMER:
//This is a port of Eben Howard's SquidLib (in Java).
//

/**
 * This utility class contains functions for working with Collections.
 *
 * @author Eben Howard - http://squidpony.com - howard@squidpony.com
 * @author P�l Trefall - URLK - admin@ptrefall.com
 */
namespace URLK
{
    public class SCollection
    {
        private static RNG rng = new RNG();

        /**
         * Prevents any instances from being created
         */
        private SCollection()
        {
            
        }

        /**
         * Returns a random element from the provided list. If the list is empty
         * then null is returned.
         *
         * @param list
         * @return
         */
        public static T? getRandomElement<T>(List<T> list) where T : struct
        {
            if (list.Count <= 0) 
                return null;
            
            int index = rng.nextInt(list.Count);
            if (index >= list.Count)
                return null;

            return list[index];
        }

        /**
         * Returns a random elements from the provided queue. If the queue is empty
         * then null is returned.
         *
         * @param list
         * @return
         */
        public static T? getRandomElement<T>(Queue<T> list) where T : struct
        {
            if (list.Count <= 0)
                return null;

            int index = rng.nextInt(list.Count);
            if (index >= list.Count)
                return null;

            return list.ToArray()[index];
        }
    }
}