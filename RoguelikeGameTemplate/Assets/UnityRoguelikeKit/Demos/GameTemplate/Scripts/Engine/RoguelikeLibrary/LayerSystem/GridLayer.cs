using System;
using System.Collections.Generic;
using UnityEngine;

//----------------------------------------------------------------- 
//                                           D E S C R I P T I O N
//
//
public class GridLayer : ILayer
{
    //------------------------------------------------------------- 
    //                                           V A R I A B L E S
    protected Vector2 size;

    //------------------------------------------------------------- 
    //                                         P R O P E R T I E S
    public Vector2 Size { get { return size; } set { size = value; } }

    //------------------------------------------------------------- 
    //                                       C O N S T R U C T O R
    public GridLayer(string name, float depth, Vector2 size) : base(name, depth)
    {
        this.size = size;
    }

    //------------------------------------------------------------- 
    //                                   F U N C T I O N A L I T Y
    public void update()
    {
        
    }

    //------------------------------------------------------------- 
    //                   P R I V A T E   F U N C T I O N A L I T Y
    private bool is_valid(Vector3 position)
    {
        if (position.x >= Size.x || position.z >= Size.y)
        {
            Debug.LogError("The position " + position + " is not valid for grid layer " + Name);
            return false;
        }

        if (position.x < 0 || position.z < 0)
        {
            Debug.LogError("The position " + position + " is not valid for grid layer " + Name);
            return false;
        }

        return true;
    }

    private int to_index(Vector3 position)
    {
        return Convert.ToInt32((position.z * Size.x) + position.x);
    }
}
